package com.gdx.extension.example.ui.tab.container;

import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.gdx.extension.ui.color.SlideColorPicker;
import com.gdx.extension.ui.tab.TabContainer;


public class ColorContainer extends TabContainer {

    private Skin skin;
    private SlideColorPicker slideColor;
    
    public ColorContainer(Skin skin)
    {
	super(skin);
	
	this.skin = skin;

	this.slideColor = new SlideColorPicker(false, skin);
	slideColor.initialize();
	add(slideColor);
    }
    
}
