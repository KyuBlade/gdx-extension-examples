package com.gdx.extension.example.ui.list;

import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.gdx.extension.ui.list.ListRow;


public class CustomListRow extends ListRow
{
    
    private Image thumb;
    private Label colorString;
    
    public CustomListRow(Pixmap pixMap, String color, Skin skin)
    {
	super(skin);
	
	thumb = new Image(new TextureRegionDrawable(new TextureRegion(new Texture(pixMap))));
	add(thumb).padLeft(5f).expandX().left();
	colorString = new Label(color, skin);
	add(colorString).padRight(5f);
	pixMap.dispose();
    }

}