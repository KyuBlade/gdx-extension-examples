package com.gdx.extension.example;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.SkinLoader;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.gdx.extension.screen.ScreenManager;

public class ApplicationCore extends ApplicationAdapter {

    private AssetManager assetManager;
    private ScreenManager screenManager;

    private static ApplicationCore instance;

    public ApplicationCore() {

    }

    @Override
    public void create() {
	super.create();

	Gdx.app.setLogLevel(Application.LOG_DEBUG);

	assetManager = new AssetManager();
	assetManager.load("skin/default.json", Skin.class, new SkinLoader.SkinParameter(
		"skin/skin.atlas"));
	assetManager.finishLoading();

	Skin _skin = assetManager.get("skin/default.json");

	Stage _stage = new Stage(new ScreenViewport(), new SpriteBatch());
	InputMultiplexer _inputProcessor = new InputMultiplexer();
	Gdx.input.setInputProcessor(_inputProcessor);

	screenManager = new ScreenManager(_stage, _skin, _inputProcessor);

	screenManager.registerScreen(new TestScreen(screenManager));
    }

    public static ApplicationCore getInstance() {
	if (instance == null) {
	    instance = new ApplicationCore();
	}

	return instance;
    }

    @Override
    public void render() {
	Gdx.gl.glClearColor(0f, 0f, 0f, 1f);
	Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

	screenManager.render(Gdx.graphics.getDeltaTime());
    }

    @Override
    public void resize(int width, int height) {
	screenManager.resize(width, height);
    }

    @Override
    public void pause() {
	screenManager.pause();
    }

    @Override
    public void resume() {
	screenManager.resume();
    }

    @Override
    public void dispose() {
	screenManager.dispose();
    }

}
